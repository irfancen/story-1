window.smoothScroll = function(target) {
    var scrollContainer = target;
    do { // find scrollContainer
        scrollContainer = scrollContainer.parentNode;
        if (!scrollContainer) return;
        scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop == 0);

    var targetY = 0;
    do { // find top of target relative to the container
        if (target == document.getElementById('home')) {
            target.offsetParent = 0;
            break;
        }
        if (target == scrollContainer) break;
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    scroll = function(c, a, b, i) {
        i += 0.5;
        if (i > 30) return;
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function() {
            scroll(c, a, b, i);
        }, 5)
    }
    // Scroll
    scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
}